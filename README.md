## terrarium
Java library to read / write [terrarium formatted](https://mapzen.com/documentation/terrain-tiles/formats/#terrarium) PNG files

## Installation

See [GitLab Package Registry](https://gitlab.com/openstreetcraft/terrarium/-/packages/22180652).

## Usage
### Read/Write pixels

Create a TerrariumImage from BufferedImage

```
import java.awt.image.BufferedImage;

BufferedImage bufferedImage = ...
TerrariumImage image = new TerrariumImage(bufferedImage);
```

Read pixel as double value

```
TerrariumValue pixel = image.getValue(4, 10);
double value = pixel.toDouble();
```

Write double value to image

```
double value = ...
TerrariumValue pixel = new TerrariumValue(value);
image.setValue(4, 10, pixel);
```

Read block of pixels as double values

```
int width = 10;
int height = 20;
TerrariumValue[] pixels = image.getValues(4, 10, width, height);
```

pixel can be accessed in this way:

```
TerrariumValue pixel = pixels[x + y * width]; 
```

Write block of double values to image

```
TerrariumValue[] pixels = ...
image.setValues(x, y, w, h, pixels);
```

### Interpolation

Create a InterpolatingTerrariumImage from BufferedImage

```
import java.awt.image.BufferedImage;

BufferedImage bufferedImage = ...
InterpolatingTerrariumImage image = new InterpolatingTerrariumImage(bufferedImage);
```

Read pixel at sub-pixel coordinate

```
TerrariumValue pixel = image.getValue(4.5, 10.2);
```

## License

[GNU LGPL Version 3](https://www.gnu.org/licenses/lgpl-3.0.html)
