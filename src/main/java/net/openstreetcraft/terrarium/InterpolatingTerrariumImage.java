package net.openstreetcraft.terrarium;

import java.awt.image.BufferedImage;

import org.apache.commons.math3.analysis.interpolation.BicubicInterpolator;
import org.apache.commons.math3.analysis.interpolation.BivariateGridInterpolator;

public class InterpolatingTerrariumImage extends TerrariumImage {
  private final Interpolator interpolator;

  public InterpolatingTerrariumImage(BufferedImage image) {
    this(image, new BicubicInterpolator());
  }

  public InterpolatingTerrariumImage(BufferedImage image, BivariateGridInterpolator gridInterpolator) {
    super(image);
    this.interpolator = new Interpolator(this, gridInterpolator);
  }

  /**
   * @param x the X coordinate of the sub-pixel
   * @param y the Y coordinate of the sub-pixel
   * 
   * @see BufferedImage#getRGB(int, int)
   */
  public TerrariumValue getValue(double x, double y) {
    double pixel = interpolator.value(x, y);
    return new TerrariumValue(pixel);
  }

  public Interpolator getInterpolator() {
    return interpolator;
  }

}
