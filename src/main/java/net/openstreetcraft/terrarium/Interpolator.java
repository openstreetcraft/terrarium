package net.openstreetcraft.terrarium;

import java.util.stream.IntStream;

import org.apache.commons.math3.analysis.BivariateFunction;
import org.apache.commons.math3.analysis.interpolation.BivariateGridInterpolator;

public class Interpolator implements BivariateFunction {
  
  private final BivariateFunction function;
  
  Interpolator(TerrariumImage image, BivariateGridInterpolator interpolator) {
    int width = image.getImage().getWidth();
    int height = image.getImage().getHeight();

    double[] xval = new double[width];
    double[] yval = new double[height];
    double[][] fval = new double[width][height];
    
    IntStream.rangeClosed(0, height - 1).parallel().forEach(y -> {
      yval[y] = y;
    });
    
    IntStream.rangeClosed(0, width - 1).parallel().forEach(x -> {
      xval[x] = x;
      IntStream.rangeClosed(0, height - 1).parallel().forEach(y -> {
        fval[x][y] = image.getValue(x, y).toDouble();
      });
    });
    
    function = interpolator.interpolate(xval, yval, fval);
  }
  
  public double[] values(double[] x, double[] y) {
    double[] out = new double[x.length];
    IntStream.rangeClosed(0, out.length - 1).parallel().forEach(n -> {
      out[n] = value(x[n], y[n]);
    });
    return out;
  }

  @Override
  public double value(double x, double y) {
    try {
      return function.value(x, y);
    } catch (Exception exception) {
      return Double.NaN;
    }
  }

}
