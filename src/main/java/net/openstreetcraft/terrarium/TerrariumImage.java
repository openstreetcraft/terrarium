package net.openstreetcraft.terrarium;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.stream.IntStream;

public class TerrariumImage {
  private final BufferedImage image;

  public TerrariumImage(BufferedImage image) {
    this.image = image;
  }

  /**
   * @param x the X coordinate of the pixel
   * @param y the Y coordinate of the pixel
   * 
   * @see BufferedImage#getRGB(int, int)
   */
  public TerrariumValue getValue(int x, int y) {
    return new TerrariumValue(getColor(x, y));
  }

  private Color getColor(int x, int y) {
    return new Color(image.getRGB(x, y));
  }
  
  /**
   * @param x the X coordinate of the pixel
   * @param y the Y coordinate of the pixel
   * @param w the width of region 
   * @param h the height of region 
   * 
   * @see BufferedImage#getRGB(int, int, int, int, int[], int, int)
   */
  public TerrariumValue[] getValues(int x, int y, int w, int h) {
    int[] buf = new int[w * h];
    image.getRGB(x, y, w, h, buf, 0, w);
    TerrariumValue[] values = new TerrariumValue[buf.length];
    IntStream.rangeClosed(0, buf.length - 1).parallel().forEach(n -> {
      Color color = new Color(buf[n]);
      values[n] = new TerrariumValue(color);
    });
    return values;
  }

  /**
   * @param x the X coordinate of the pixel
   * @param y the Y coordinate of the pixel
   * @param value the terrarium value
   * 
   * @see BufferedImage#setRGB(int, int, int)
   */
  public void setValue(int x, int y, TerrariumValue value) {
    setColor(x, y, value.toColor());
  }

  private void setColor(int x, int y, Color color) {
    image.setRGB(x, y, color.getRGB());
  }

  /**
   * @param x the X coordinate of the pixel
   * @param y the Y coordinate of the pixel
   * @param w the width of region 
   * @param h the height of region 
   * @param values w * h array of Terrarium values
   * 
   * @see BufferedImage#setRGB(int, int, int, int, int[], int, int)
   */
  public void setValues(int x, int y, int w, int h, TerrariumValue[] values) {
    int[] buf = new int[w * h];
    IntStream.rangeClosed(0, buf.length - 1).parallel().forEach(n -> {
      buf[n] = values[n].toColor().getRGB();
    });
    image.setRGB(x, y, w, h, buf, 0, w);
  }

  public BufferedImage getImage() {
    return image;
  }
}
