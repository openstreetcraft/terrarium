package net.openstreetcraft.terrarium;

import java.awt.Color;
import java.util.Objects;

/**
 * Terrarium format PNG tiles contain raw elevation data in meters, in Web
 * Mercator projection (EPSG:3857). All values are positive with a 32768
 * offset, split into the red, green, and blue channels, with 16 bits of integer
 * and 8 bits of fraction.
 */
public class TerrariumValue {
  
  public static TerrariumValue NODATA = new TerrariumValue(0, 0, 0);
  
  int r;
  int g;
  int b;

  public TerrariumValue(float value) {
    value += 32768.0f;
    r = (int) Math.floor(value / 256.0f);
    g = (int) Math.floor(value % 256.0f);
    b = (int) Math.floor((value - Math.floor(value)) * 256.0f);
  }

  public TerrariumValue(double value) {
    value += 32768.0;
    r = (int) Math.floor(value / 256.0);
    g = (int) Math.floor(value % 256.0);
    b = (int) Math.floor((value - Math.floor(value)) * 256.0);
  }

  public TerrariumValue(int r, int g, int b) {
    this.r = requireByte(r);
    this.g = requireByte(g);
    this.b = requireByte(b);
  }

  public TerrariumValue(Color color) {
    this.r = color.getRed();
    this.g = color.getGreen();
    this.b = color.getBlue();
  }

  private static int requireByte(int value) {
    if (value < 0 || value > 255) {
      throw new IllegalArgumentException(Integer.toString(value));
    }
    return value;
  }

  public float toFloat() {
    return (r * 256.0f + g + b / 256.0f) - 32768.0f;
  }

  public double toDouble() {
    return (r * 256.0 + g + b / 256.0) - 32768.0;
  }
  
  public Color toColor() {
    return new Color(r, g, b);
  }

  @Override
  public String toString() {
    return "rgb(" + r + ", " + g + ", " + b + ")";
  }

  @Override
  public int hashCode() {
    return Objects.hash(r, g, b);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof TerrariumValue)) {
      return false;
    }

    TerrariumValue other = (TerrariumValue) obj;

    return Objects.equals(this.r, other.r)
        && Objects.equals(this.g, other.g)
        && Objects.equals(this.b, other.b);
  }

  public int r() {
    return r;
  }

  public int g() {
    return g;
  }

  public int b() {
    return b;
  }

}
