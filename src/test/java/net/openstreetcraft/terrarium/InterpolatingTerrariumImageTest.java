package net.openstreetcraft.terrarium;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class InterpolatingTerrariumImageTest {
  
  TestResources resources = new TestResources();

  @Test
  public void getValue() {
    InterpolatingTerrariumImage image = new InterpolatingTerrariumImage(resources.testImage());
    TerrariumValue expected = new TerrariumValue(118, 166, 0);
    TerrariumValue actual = image.getValue(4.0, 10.0);
    assertEquals(expected, actual);
  }

  @Test
  public void getNaN() {
    InterpolatingTerrariumImage image = new InterpolatingTerrariumImage(resources.testImage());
    TerrariumValue expected = TerrariumValue.NODATA;
    TerrariumValue actual = image.getValue(Double.NaN, 0.0);
    assertEquals(expected, actual);
  }

  @Test
  public void getOutOfBounds() {
    InterpolatingTerrariumImage image = new InterpolatingTerrariumImage(resources.testImage());
    TerrariumValue expected = TerrariumValue.NODATA;
    TerrariumValue actual = image.getValue(1000.0, 0.0);
    assertEquals(expected, actual);
  }

}
