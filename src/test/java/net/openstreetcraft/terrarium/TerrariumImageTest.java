package net.openstreetcraft.terrarium;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TerrariumImageTest {

  TestResources resources = new TestResources();

  @Test
  public void getValue() {
    TerrariumImage image = new TerrariumImage(resources.testImage());
    TerrariumValue expected = new TerrariumValue(118, 166, 0);
    TerrariumValue actual = image.getValue(4, 10);
    assertEquals(expected, actual);
  }

  @Test
  public void setValue() {
    TerrariumImage image = new TerrariumImage(resources.testImage());
    TerrariumValue expected = new TerrariumValue(1, 2, 3);
    image.setValue(4, 10, expected);
    TerrariumValue actual = image.getValue(4, 10);
    assertEquals(expected, actual);
  }

  @Test
  public void getValues() {
    TerrariumImage image = new TerrariumImage(resources.testImage());
    TerrariumValue[] expected = {
        new TerrariumValue(118, 166, 0),
        new TerrariumValue(118, 105, 0),
        new TerrariumValue(118, 57, 0),
        new TerrariumValue(117, 235, 0),
    };

    TerrariumValue[] actual = image.getValues(4, 10, 2, 2);
    assertArrayEquals(expected, actual);

    assertEquals(expected[0], image.getValue(4, 10));
    assertEquals(expected[1], image.getValue(5, 10));
    assertEquals(expected[2], image.getValue(4, 11));
    assertEquals(expected[3], image.getValue(5, 11));
  }

  @Test
  public void setValues() {
    TerrariumImage image = new TerrariumImage(resources.testImage());
    TerrariumValue[] expected = {
        new TerrariumValue(1, 2, 3),
        new TerrariumValue(4, 5, 6),
        new TerrariumValue(7, 8, 9),
        new TerrariumValue(8, 7, 6),
    };
    image.setValues(4, 10, 2, 2, expected);
    TerrariumValue[] actual = image.getValues(4, 10, 2, 2);
    assertArrayEquals(expected, actual);
  }

}
