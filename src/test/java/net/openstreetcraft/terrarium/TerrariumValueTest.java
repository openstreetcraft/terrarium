package net.openstreetcraft.terrarium;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TerrariumValueTest {

  @Test
  public void decodesNoDataValue() {
    TerrariumValue value = new TerrariumValue(-32768.0);
    assertEquals(TerrariumValue.NODATA, value);
  }

  @Test
  public void decodesDoubleValue() {
    TerrariumValue value = new TerrariumValue(2523.266);
    
    assertEquals(137, value.r());
    assertEquals(219, value.g());
    assertEquals(68, value.b());
  }

  @Test
  public void decodesFloatValue() {
    TerrariumValue value = new TerrariumValue(2523.266f);
    
    assertEquals(137, value.r());
    assertEquals(219, value.g());
    assertEquals(68, value.b());
  }

  @Test
  public void decodesRGBValue() {
    TerrariumValue value = new TerrariumValue(137, 219, 68);
    
    assertEquals(137, value.r());
    assertEquals(219, value.g());
    assertEquals(68, value.b());
  }

  @Test
  public void encodesDoubleValue() {
    TerrariumValue value = new TerrariumValue(137, 219, 68);
    
    assertEquals(2523.265625, value.toDouble(), Double.MIN_VALUE);
  }

  @Test
  public void encodesFloatValue() {
    TerrariumValue value = new TerrariumValue(137, 219, 68);
    
    assertEquals(2523.265625f, value.toFloat(), Float.MIN_VALUE);
  }
}
