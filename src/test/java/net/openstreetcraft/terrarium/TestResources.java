package net.openstreetcraft.terrarium;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class TestResources {

  public BufferedImage testImage() {
    try (InputStream resource = getClass().getResourceAsStream("/terrarium256.png")) {
      return ImageIO.read(resource);
    } catch (IOException exception) {
      throw new AssertionError();
    }    
  }

}
